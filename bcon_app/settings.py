# SPDX-License-Identifier: GPL-2.0-or-later

# Simple place to gather app-wide settings.
import os.path


################################
# General project-wide settings.

# NOTE: All of these settings are expected to be fully static, DO NOT modify them at runtime.

# BCon year!
BCON_YEAR = 2024

# Whether to enable some debug/dev options in the code & UI. Disable for production.
IS_DEBUG = True

# URL to get reference data (presentations JSON database) from.
REFERENCE_DATA_JSON_URL = f"https://conference.blender.org/{BCON_YEAR}/presentations/json/"


# The fields are taken from the presentation's info in presentations.json, through usage of BConVideoDataTalk data.
VIDEO_DESCRIPTION = f'''"{{title}}" by {{speakers}}
Blender Conference {BCON_YEAR}
{{day}} {{start_time}} at the {{location}}.
'''
VIDEO_LOCATION = "Felix Meritis, Amsterdam, {location}"

# Path to root directory containing some private info related to the conference, among other things:
#  - The authentification data.
# WARNING! Contains sensitive data that should not be publicly available in any way!
APP_DATA_ROOT = "/home/guest/blender/bcon_app/data_runtime"

# Path to root directory containing the essential info related to the current conference, among other things:
#  - The reference JSon database of all talks.
#  - The reference JSon settings.
# NOTE: This is also typically where the working video files will be put during video processing.
WORK_DATA_ROOT = "/media/guest/Data/bcon_2024"


###########################################
# Settings related to reference talks data.

# Talk categories that actually need to be processed (i.e. that generates video to upload on the BCon playlist).
VALID_TALK_CATEGORIES = {"Talk", "Live Tutorial", "Other",}

# Talk which titles partially match with one of these strings will also be considered as not requiring any video processing.
INVALID_TALK_TITLE_MATCH = {"Live Feed"}


########################
# Video Timing Settings.

# Expected FPS for whole videos.
FPS = 25.0
# Expected sample rate for audio.
AUDIO_SAMPLE_RATE = 48000
AUDIO_BLENDER_SAMPLE_RATE = 'RATE_48000'
# Delay in audio caused by 'priming samples', needs to be compensated in audio part of complex filter in some cases.
# Value in seconds.
# See https://stackoverflow.com/questions/42410479/ffmpeg-wrong-audio-file-after-conversion-in-aac
AUDIO_PRIMING_SAMPLE_CORRECTION = (1024 / AUDIO_SAMPLE_RATE)

# Time (in seconds) of fully showing the title card.
TITLECARD_LENGTH = 2.0
# Time (in seconds) for the crossfade between the title card and the main video.
TITLECARD_XFADE = 1.0


##########################
# Youtube upload settings.

YOUTUBE_PLAYLIST_ID = 'PLa1F2ddGya_-Ymw4YlOjqrdQxiRMJql5x'  # BCon2024


#####################################
# Evil installer of all dependencies!

# NOTE: Will request root (sudo) access too (in console!).
def ensure_dependencies():
    print("Ensuring all required python and system dependencies are installed...")
    print("NOTE: Target system: Linux Debian testing, Blender main.")
    print("NOTE: This likely will need to be updated depending on target system for this app.")

    import subprocess
    import sys
    subprocess.run((sys.executable, "-m", "pip", "install", "google-api-python-client", "httplib2", "oauth2client"))

    subprocess.run(("sudo", "apt", "install", "ffmpeg", "mediainfo", ""))


