#!/usr/bin/env python3

if "settings" in locals():
    import importlib
    importlib.reload(settings)
else:
    from . import settings
if "reports" in locals():
    import importlib
    importlib.reload(reports)
else:
    from . import reports


import http.client
import httplib2
import os
import pprint
import random
import sys
import time
import webbrowser


from apiclient.discovery import build, Resource
from apiclient.errors import HttpError
from apiclient.http import MediaFileUpload
from oauth2client.client import flow_from_clientsecrets
from oauth2client.file import Storage
from oauth2client.tools import argparser, run_flow


YOUTUBE_PLAYLIST_ID = settings.YOUTUBE_PLAYLIST_ID
APP_DATA_ROOT = settings.APP_DATA_ROOT


# Much of this code was taken from the example provided by Google/YouTube themselves at
# https://developers.google.com/youtube/v3/guides/uploading_a_video


# Explicitly tell the underlying HTTP transport library not to retry, since
# we are handling retry logic ourselves.
httplib2.RETRIES = 1

# Maximum number of times to retry before giving up.
MAX_RETRIES = 10

# Always retry when these exceptions are raised.
RETRIABLE_EXCEPTIONS = (httplib2.HttpLib2Error, IOError, http.client.NotConnected,
                        http.client.IncompleteRead, http.client.ImproperConnectionState,
                        http.client.CannotSendRequest, http.client.CannotSendHeader,
                        http.client.ResponseNotReady, http.client.BadStatusLine)

# Always retry when an apiclient.errors.HttpError with one of these status
# codes is raised.
RETRIABLE_STATUS_CODES = [500, 502, 503, 504]

# The CLIENT_SECRET_FILE variable specifies the name of a file that contains
# the OAuth 2.0 information for this application, including its client_id and
# client_secret. You can acquire an OAuth 2.0 client ID and client secret from
# the Google API Console at
# https://console.developers.google.com/.
# Please ensure that you have enabled the YouTube Data API for your project.
# For more information about using OAuth2 to access the YouTube Data API, see:
#   https://developers.google.com/youtube/v3/guides/authentication
# For more information about the client_secrets.json file format, see:
#   https://developers.google.com/api-client-library/python/guide/aaa_client_secrets
CLIENT_SECRET_FILE = "client_secret.json"
CLIENT_SECRET_PATH = os.path.abspath(
    os.path.join(APP_DATA_ROOT, CLIENT_SECRET_FILE)
)

# This OAuth 2.0 access scope allows an application to upload files to the
# authenticated user's YouTube channel, but doesn't allow other types of access.
YOUTUBE_UPLOAD_SCOPE = "https://www.googleapis.com/auth/youtube.upload"
YOUTUBE_API_SERVICE_NAME = "youtube"
YOUTUBE_API_VERSION = "v3"

# Used for adding the video to a playlist AND also suitable for uploading videos:
YOUTUBE_SCOPE = "https://www.googleapis.com/auth/youtube"


# This variable defines a message to display if the CLIENT_SECRET_FILE is
# missing.
MISSING_CLIENT_SECRETS_MESSAGE = """
WARNING: Please configure OAuth 2.0

To make this sample run you will need to populate the client_secrets.json file
found at:

   %s

with information from the API Console
https://console.developers.google.com/

For more information about the client_secrets.json file format, please visit:
https://developers.google.com/api-client-library/python/guide/aaa_client_secrets
""" % CLIENT_SECRET_PATH

VALID_PRIVACY_STATUSES = {v:v for v in ("public", "private", "unlisted")}


class UploadVideo:
    def __init__(self, bl_talk):
        self.uid = bl_talk.uid
        self.title = bl_talk.title
        self.start_time = bl_talk.timestamp_start
        self.filename = bl_talk.export_path
        self.thumbnail_filename = bl_talk.thumbnail_path
        # Replace <> as they are not allowed by YouTube.
        # And no, escaping them to &lt; and &gt; doesn't work, but ‹› does.
        self.description = bl_talk.description.replace('<', '‹').replace('>', '›')
        self.location = bl_talk.location_full

    @staticmethod
    def get_authenticated_service() -> Resource:
        flow = flow_from_clientsecrets(CLIENT_SECRET_PATH,
                                       scope=YOUTUBE_SCOPE,
                                       message=MISSING_CLIENT_SECRETS_MESSAGE)

        oauth_storage_path = os.path.join(APP_DATA_ROOT, "bcon-app-oauth2.json")
        storage = Storage(oauth_storage_path)
        credentials = storage.get()

        if credentials is None or credentials.invalid:
            credentials = run_flow(flow, storage)

        return build(YOUTUBE_API_SERVICE_NAME, YOUTUBE_API_VERSION,
                     http=credentials.authorize(httplib2.Http()))

    # This method implements an exponential backoff strategy to resume a failed upload.
    # `validation_fields` is used to validate the answer for the server (i.e. decide if the request was successful).
    #                     it defines the keys expected in the answer, and optionaly their expected value (if not None).
    @staticmethod
    def resumable_upload(request, validation_fields: dict) -> dict:
        response = None
        retry = 0

        for _ in range(2 * MAX_RETRIES):  # Just to avoid infinite queries to YouTube in case of bugs.
            error = ''
            try:
                status, response = request.next_chunk()
                if response is not None:
                    for key, value in validation_fields.items():
                        if key not in response:
                            raise SystemExit(
                                f"The upload failed with an unexpected response, key '{key}' is missing: {response}")
                        if value is not None and response[key] != value:
                            raise SystemExit(
                                f"The upload failed with an unexpected response, "
                                f"invalid '{key}' value (expected {value}, got {response[key]}): {response}")
                    print(f"Request {response.get('kind')} has been successful.")
                    return response
            except HttpError as e:
                if e.resp.status not in RETRIABLE_STATUS_CODES:
                    raise
                error = "A retriable HTTP error %d occurred:\n%s" % (e.resp.status, e.content)
            except RETRIABLE_EXCEPTIONS as e:
                error = "A retriable error occurred: %s" % e

            if error:
                print(error)
                retry += 1
                if retry > MAX_RETRIES:
                    raise SystemExit("No longer attempting to retry.")

                max_sleep = 2 ** retry
                sleep_seconds = random.random() * max_sleep
                print("Sleeping %f seconds and then retrying..." % sleep_seconds)
                time.sleep(sleep_seconds)

        return response

    def initialize_upload(self, youtube: Resource) -> dict:
        """Upload the video and pass the metadata.

        :returns: the video metadata from YouTube, see https://developers.google.com/youtube/v3/docs/videos
        """
        start_time = time.localtime(self.start_time)

        # See https://developers.google.com/youtube/v3/docs/videos
        body = {
            'snippet': {
                'title': self.title,
                'description': self.description,
                'recording_date': time.strftime("%Y-%m-%d", start_time),
                'location': self.location,
                # TODO: test if this works for live video premier
                # 'liveBroadcastContent': 'live',
            },
            'status': {
                # Upload as private
                'privacyStatus': VALID_PRIVACY_STATUSES["private"],
                'license': 'creativeCommon',
                'embeddable': True,
            },
        }
        print('Inserting:')
        pprint.pprint(body)

        # Call the API's videos.insert method to create and upload the video.
        insert_request = youtube.videos().insert(
            part=",".join(list(body.keys())),
            body=body,
            notifySubscribers=False,
            # The chunksize parameter specifies the size of each chunk of data, in
            # bytes, that will be uploaded at a time. Set a higher value for
            # reliable connections as fewer chunks lead to faster uploads. Set a lower
            # value for better recovery on less reliable connections.
            #
            # Setting "chunksize" equal to -1 in the code below means that the entire
            # file will be uploaded in a single HTTP request. (If the upload fails,
            # it will still be retried where it left off.) This is usually a best
            # practice, but if you're using Python older than 2.6 or if you're
            # running on App Engine, you should set the chunksize to something like
            # 1024 * 1024 (1 megabyte).
            media_body=MediaFileUpload(self.filename, chunksize=-1, resumable=True)
        )

        print("Uploading file...")
        return self.resumable_upload(insert_request, {'kind': "youtube#video", 'id': None})

    def thumbnail_set(self, youtube: Resource, video_info: dict) -> dict:
        """Upload and set the thumbnail for the given video info.

        :returns: the thumbnail metadata from YouTube, see https://developers.google.com/youtube/v3/docs/thumbnails
        """

        # Call the API's thumbnails.set method to upload the thumbnail and set it for the video.
        thumbnail_set_request = youtube.thumbnails().set(
            videoId=video_info['id'],
            # See #initialize_upload function above for details about this call and its parameters.
            media_body=MediaFileUpload(self.thumbnail_filename, chunksize=-1, resumable=True)
        )

        print("Uploading thumbnail...")
        return self.resumable_upload(thumbnail_set_request, {'kind': "youtube#thumbnailSetResponse", 'items': None})

    @staticmethod
    def add_to_playlist(youtube: Resource, video_info: dict) -> dict:
        """Add a video to the playlist identified by YOUTUBE_PLAYLIST_ID.

        :returns: the response JSON.
        """

        body = {
            'snippet': {
                'title': video_info['snippet']['title'],
                'description': video_info['snippet']['description'],
                'thumbnails': video_info['snippet']['thumbnails'],
                'channelTitle': video_info['snippet']['channelTitle'],
                'playlistId': YOUTUBE_PLAYLIST_ID,
                'resourceId': {
                    'kind': video_info['kind'],
                    'videoId': video_info['id'],
                },
            },

            'contentDetails': {
                'videoId': video_info['id'],
            },
        }

        insert_request = youtube.playlistItems().insert(
            part=",".join(list(body.keys())),
            body=body,
        )
        response = insert_request.execute(num_retries=MAX_RETRIES)
        return response

    def upload_video(self, reports):
        youtube = self.get_authenticated_service()

        try:
            video_info = self.initialize_upload(youtube)
        except HttpError as e:
            raise SystemExit("An HTTP error %d occurred while uploading:\n%s" %
                             (e.resp.status, e.content.decode()))
        print('Video data:')
        pprint.pprint(video_info)
        print()

        try:
            thumbnail_info = self.thumbnail_set(youtube, video_info)
        except HttpError as e:
            raise SystemExit("An HTTP error %d occurred while setting thumbnail:\n%s" %
                             (e.resp.status, e.content.decode()))
        print('Thumbnail data:')
        pprint.pprint(video_info)
        print()

        playlist_info = self.add_to_playlist(youtube, video_info)
        print('Playlist item data:')
        pprint.pprint(playlist_info)
        print()

        video_url = f"https://www.youtube.com/watch?v={video_info['id']}&list={playlist_info['id']}"
        video_edit_url = f"https://studio.youtube.com/video/{video_info['id']}/edit"
        print(f'Video available at: {video_url}')
        print()

        return video_url, video_edit_url
