# SPDX-License-Identifier: GPL-2.0-or-later


from . import (
    settings,
    stages,
    bl_data,
)
if "bpy" in locals():
    import importlib
    if "settings" in locals():
        importlib.reload(settings)
    if "stages" in locals():
        importlib.reload(stages)
    if "bl_data" in locals():
        importlib.reload(bl_data)


import bpy
from bpy.props import (
        StringProperty,
        BoolProperty,
        FloatProperty,
        IntProperty,
        EnumProperty,
        CollectionProperty,
        PointerProperty,
        )


import json
import os
import shutil
import subprocess
import webbrowser


AUDIO_BLENDER_SAMPLE_RATE = settings.AUDIO_BLENDER_SAMPLE_RATE
REFERENCE_DATA_JSON_URL = settings.REFERENCE_DATA_JSON_URL


def bcon_reference_data_download(dst_path):
    dst_path_tmp = dst_path + ".tmp"
    args = (
        "wget",
        "-O", dst_path_tmp,
        REFERENCE_DATA_JSON_URL,
    )
    result = subprocess.run(args, capture_output=True)
    print(result.stdout)
    print(result.stderr)
    if b"200 OK" not in result.stderr:
        print("No '200 OK'")
        return False
    with open(dst_path_tmp) as f:
        try:
            json.load(f)
        except Exception as e:
            print(repr(e))
            return False
    if os.path.exists(dst_path):
        os.remove(dst_path)
    os.rename(dst_path_tmp, dst_path)
    return True


class BConVideo_install_dependencies(bpy.types.Operator):
    """Install dependencies required by the BCon Video App"""
    bl_idname = "sequencer.bcon_video_install_dependencies"
    bl_label = "! Install Dependencies !"

    def draw(self, context):
        layout = self.layout
        layout.alert = True
        layout.label(text="!!! WARNING !!!")
        layout.label(text="This operator will install required components on this system.")
        layout.label(text="Including apps like ffmpeg or mediainfo, using APT (requires the `sudo` password in the system console).")
        layout.label(text="And Python modules (like the Google API suite), using pip (in this Blender python installation).")

    def execute(self, context):
        settings.ensure_dependencies()

        return {'FINISHED'}
        
    def invoke(self, context, event):
        wm = context.window_manager
        return wm.invoke_props_dialog(self, width=600)


class BConVideo_reset(bpy.types.Operator):
    """BCon Video App initialization"""
    bl_idname = "sequencer.bcon_video_reset"
    bl_label = "Reset"

    @classmethod
    def poll(cls, context):
        bcon_data = context.scene.bcon_video_data
        if not bcon_data.paths.raw_videos_dir or not bcon_data.paths.reference_path:
            return False
        return True

    def execute(self, context):
        # Ensure valid video settings for all scenes.
        for scene in bpy.data.scenes:
            scene.render.resolution_x = 1920
            scene.render.resolution_y = 1080
            scene.render.pixel_aspect_x = 1.0
            scene.render.pixel_aspect_y = 1.0
            scene.render.fps = 25
            scene.render.fps_base = 1.0
            vse = scene.sequence_editor
            if vse:
                vse.display_stack(None)
                while len(vse.sequences):
                    seq = vse.sequences[0]
                    vse.sequences.remove(seq)
                for idx, channel in enumerate(vse.channels):
                    channel.name = f"Channel {idx}"
                    channel.mute = True
                    channel.lock = False

        context.preferences.system.audio_sample_rate = AUDIO_BLENDER_SAMPLE_RATE

        bcon_data = context.scene.bcon_video_data

        bcon_data.talks.clear()
        stages.TalksManager().clear()
        if bcon_data.paths.reference_path:
            bcon_reference_data_download(bcon_data.paths.reference_path)
            bcon_data.update_from_reference_data()

        bl_data.update_timer_kick(None, context)

        return {'FINISHED'}


class BConVideo_reload(bpy.types.Operator):
    """BCon Video reload of backed-up info, and reset/recover of active talks processing"""
    bl_idname = "sequencer.bcon_video_reload"
    bl_label = "Reload"

    @classmethod
    def poll(cls, context):
        bcon_data = context.scene.bcon_video_data
        if not bcon_data.paths.persistent_path or not bcon_data.paths.reference_path:
            return False
        return True

    def execute(self, context):
        bcon_data = context.scene.bcon_video_data
        talks_manager = stages.TalksManager()
        talks_manager.clear()

        if not bcon_data.paths.reference_path or not bcon_data.paths.persistent_path:
            return {'CANCELLED'}

        bcon_reference_data_download(bcon_data.paths.reference_path)
        bcon_data.update_from_persistent_data()
        bcon_data.update_from_reference_data()
        bcon_data.reset_talks_manager(context)

        bl_data.update_timer_kick(None, context)

        return {'FINISHED'}


class BConVideo_stage_next(bpy.types.Operator):
    bl_idname = "sequencer.bcon_video_stage_next"
    bl_label = "Next Stage"

    @classmethod
    def poll(cls, context):
        data = context.scene.bcon_video_data
        talk = data.talks[data.talk_active_index]
        talks_manager = stages.TalksManager()
        if talks_manager.talk_is_active(talk):
            return talks_manager.talk_next_stage_poll(context, talk)
        else:
            return talk.stage in {stages.STAGE_UPCOMING, stages.STAGE_DONE}

    def execute(self, context):
        data = context.scene.bcon_video_data
        talk = data.talks[data.talk_active_index]
        talks_manager = stages.TalksManager()
        if not talks_manager.talk_is_active(talk):
            talks_manager.talk_init(context, talk)
        talks_manager.talk_next_stage(context, talk)

        bl_data.update_timer_kick(None, context)

        return {'FINISHED'}


class BConVideo_stage_prev(bpy.types.Operator):
    bl_idname = "sequencer.bcon_video_stage_prev"
    bl_label = "Prev Stage"

    @classmethod
    def poll(cls, context):
        data = context.scene.bcon_video_data
        talk = data.talks[data.talk_active_index]
        talks_manager = stages.TalksManager()
        return talks_manager.talk_is_active(talk) and talks_manager.talk_prev_stage_poll(context, talk)

    def execute(self, context):
        data = context.scene.bcon_video_data
        talk = data.talks[data.talk_active_index]
        talks_manager = stages.TalksManager()
        if not talks_manager.talk_is_active(talk):
            return {'CANCELLED'}
        talks_manager.talk_prev_stage(context, talk)

        bl_data.update_timer_kick(None, context)

        return {'FINISHED'}


class BConVideo_frame_go(bpy.types.Operator):
    bl_idname = "sequencer.bcon_video_frame_go"
    bl_label = "Go To Frame"

    frame: IntProperty(
        name="Frame",
        description="Scene frame to go to",
    )

    @classmethod
    def poll(cls, context):
        data = context.scene.bcon_video_data
        talk = data.talks[data.talk_active_index]
        talks_manager = stages.TalksManager()
        return talks_manager.talk_is_active(talk)

    def execute(self, context):
        context.scene.frame_current = self.frame
        return {'FINISHED'}


class BConVideo_stage_acquire_files_select(bpy.types.Operator):
    bl_idname = "sequencer.bcon_video_acquire_files_select"
    bl_label = "Acquire Files"
    bl_description = "Select video files to acquire for the active talk"

    directory: StringProperty()

    files: CollectionProperty(
            name="File Paths",
            type=bpy.types.OperatorFileListElement,
            )

    filepath: StringProperty(
        name="File Path",
        description="Filepath used for importing the file",
        maxlen=1024,
        subtype='FILE_PATH',
    )

    filter_movie: BoolProperty(name="FilterMovie", default=True,)
    filter_folder: BoolProperty(name="FilterFolder", default=True,)

    @classmethod
    def poll(cls, context):
        data = context.scene.bcon_video_data
        talk = data.talks[data.talk_active_index]
        talks_manager = stages.TalksManager()
        return talk.work_dir and talk.stage == stages.STAGE_ACQUIRING and talks_manager.talk_is_active(talk)

    def invoke(self, context, _event):
        context.window_manager.fileselect_add(self)
        return {'RUNNING_MODAL'}

    def execute(self, context):
        data = context.scene.bcon_video_data
        talk = data.talks[data.talk_active_index]
        talks_manager = stages.TalksManager()
        if not talks_manager.talk_is_active(talk):
            return {'CANCELLED'}
        if not self.files:
            return {'CANCELLED'}
        if not talk.work_dir:
            return {'CANCELLED'}

        ret = {'CANCELLED'}
        dirname = self.directory
        for file_path in self.files:
            path_src = os.path.join(dirname, file_path.name)
            raw_video = talk.raw_videos.add()
            talk.raw_videos_counter += 1
            raw_video.filename = f"{talk.uid}_{talk.raw_videos_counter:02}_{os.path.basename(path_src)}"[:32]
            raw_video.path_src = path_src
            raw_video.path_work = os.path.join(talk.work_dir, raw_video.filename)
            ret = {'FINISHED'}
        return ret


class BConVideo_talk_file_remove(bpy.types.Operator):
    bl_idname = "sequencer.bcon_video_talk_file_remove"
    bl_label = "Remove File"
    bl_description = "Remove given file (video sequence) from the active talk"

    filename: StringProperty(
        name="File name",
        description="Unique filename identifier to remove from this talk",
        maxlen=32,
    )

    @classmethod
    def poll(cls, context):
        data = context.scene.bcon_video_data
        talk = data.talks[data.talk_active_index]
        return talk and talk.raw_videos

    def execute(self, context):
        scene = context.scene
        data = scene.bcon_video_data
        talk = data.talks[data.talk_active_index]
        talks_manager = stages.TalksManager()
        if not talks_manager.talk_is_active(talk):
            return {'CANCELLED'}
        if not self.filename or self.filename not in talk.raw_videos:
            return {'CANCELLED'}

        # Note that only indices can be used to remove items from a py-defined CollectionProperty.
        rvid = None
        rvid_idx = -1
        for idx, (item_k, item_v) in enumerate(talk.raw_videos.items()):
            if item_k == self.filename:
                rvid = item_v
                rvid_idx = idx
                break
        assert rvid is not None

        vse = scene.sequence_editor
        if not vse:
            return {'FINISHED'}
        vse.display_stack(None)
        meta = None
        if talk.uid in vse.sequences:
            meta = vse.sequences[talk.uid]
            assert meta.type == 'META'
            if rvid.filename in meta.sequences:
                rvid_seq = meta.sequences[rvid.filename]
                meta.sequences.remove(rvid_seq)
            else:
                print(meta.sequences[:])
                print("No matching VSE seq for raw video", rvid.filename)

        talk.raw_videos.remove(rvid_idx)
        vse.display_stack(meta)
        return {'FINISHED'}


class BConVideo_stage_processing_cutframe_set(bpy.types.Operator):
    bl_idname = "sequencer.bcon_video_processing_cutframe_set"
    bl_label = "Set/Clear Cut Frame"
    bl_description = "Use current scene's frame to set cut-in/-out timestamps of the specified video, or clear them"

    raw_video_uid: StringProperty(
        name="Raw Video UID",
        description="UID of the affected raw video",
    )

    prop_set: EnumProperty(
        name="Property to set",
        description="Which property to define from current scene's frame",
        items=(
            ('CUT_IN', "Cut In", "Cut-in timestamp of the raw video"),
            ('CUT_OUT', "Cut Out", "Cut-out timestamp of the raw video"),
        ),
    )

    @classmethod
    def poll(cls, context):
        scene = context.scene
        if not scene.sequence_editor:
            return False
        data = scene.bcon_video_data
        talk = data.talks[data.talk_active_index]
        talks_manager = stages.TalksManager()
        return talk.stage == stages.STAGE_PROCESSING and talks_manager.talk_is_active(talk)

    def execute(self, context):
        scene = context.scene
        data = scene.bcon_video_data
        talk = data.talks[data.talk_active_index]
        talks_manager = stages.TalksManager()
        if not talks_manager.talk_is_active(talk):
            return {'CANCELLED'}
        if self.raw_video_uid not in talk.raw_videos:
            return {'CANCELLED'}
        rvid = talk.raw_videos[self.raw_video_uid]

        if not scene.sequence_editor:
            return {'CANCELLED'}
        if talk.uid not in scene.sequence_editor.sequences:
            return {'CANCELLED'}
        meta = scene.sequence_editor.sequences[talk.uid]
        if self.raw_video_uid not in meta.sequences:
            return {'CANCELLED'}
        rvid_seq = meta.sequences[self.raw_video_uid]

        # What is current scene frame's timestamp in the afffected raw video.
        timestamp = scene.frame_current_final - rvid_seq.frame_start
        if self.prop_set == 'CUT_IN':
            if rvid.is_cut_in_set:
                rvid_seq.frame_offset_start = 0.0
                rvid.cut_in = 0.0
                rvid.is_cut_in_set = False
                rvid.is_final_set = False
            else:
                rvid_seq.frame_offset_start = timestamp
                rvid.cut_in = timestamp
                rvid.is_cut_in_set = True
        elif self.prop_set == 'CUT_OUT':
            if rvid.is_cut_out_set:
                rvid_seq.frame_offset_end = 0.0
                rvid.cut_out = 0.0
                rvid.is_cut_out_set = False
                rvid.is_final_set = False
            else:
                rvid_seq.frame_offset_end = rvid_seq.frame_duration - timestamp
                rvid.cut_out = timestamp
                rvid.is_cut_out_set = True

        return {'FINISHED'}


class BConVideo_stage_processing_thumbnail_frame_set(bpy.types.Operator):
    bl_idname = "sequencer.bcon_video_processing_thumbnail_frame_set"
    bl_label = "Set/Clear Thumbnail Frame"
    bl_description = "Use current scene's frame to set thumbnail timestamp in the final video, or clear it"

    @classmethod
    def poll(cls, context):
        scene = context.scene
        if not scene.sequence_editor:
            return False
        data = scene.bcon_video_data
        talk = data.talks[data.talk_active_index]
        talks_manager = stages.TalksManager()
        return talk.stage == stages.STAGE_PROCESSING and talks_manager.talk_is_active(talk)

    def execute(self, context):
        scene = context.scene
        data = scene.bcon_video_data
        talk = data.talks[data.talk_active_index]
        talks_manager = stages.TalksManager()
        if not talks_manager.talk_is_active(talk):
            return {'CANCELLED'}

        if not scene.sequence_editor:
            return {'CANCELLED'}
        if talk.uid not in scene.sequence_editor.sequences:
            return {'CANCELLED'}

        timestamp = scene.frame_current_final
        if talk.thumbnail_frame > 0:
            talk.thumbnail_frame = 0
        else:
            talk.thumbnail_frame = int(timestamp)

        return {'FINISHED'}


class BConVideo_stage_exporting_final_validate(bpy.types.Operator):
    bl_idname = "sequencer.bcon_video_exporting_final_validate"
    bl_label = "Validate Final Export"
    bl_description = "Validate final export by playing it in an external video player to ensure everything is OK"

    @classmethod
    def poll(cls, context):
        scene = context.scene
        if not scene.sequence_editor:
            return False
        data = scene.bcon_video_data
        talk = data.talks[data.talk_active_index]
        talks_manager = stages.TalksManager()
        return (talk.stage == stages.STAGE_EXPORTING and
                talks_manager.talk_is_active(talk) and
                talk.is_export_stage_validation_required)

    def execute(self, context):
        scene = context.scene
        data = scene.bcon_video_data
        talk = data.talks[data.talk_active_index]
        if not talk.is_export_stage_validation_required:
            return {'CANCELLED'}

        talk.is_export_stage_validation_required = False
        talk.is_export_stage_validated = True
        return {'FINISHED'}

    def invoke(self, context, event):
        import subprocess
        scene = context.scene
        data = scene.bcon_video_data
        talk = data.talks[data.talk_active_index]
        if not talk.is_export_stage_validation_required:
            return {'CANCELLED'}

        subprocess.run(("totem", talk.export_path))

        wm = context.window_manager
        return wm.invoke_props_dialog(self)


class BConVideo_stage_uploading_final_validate(bpy.types.Operator):
    bl_idname = "sequencer.bcon_video_uploading_final_validate"
    bl_label = "Validate Final Upload"
    bl_description = "Validate final upload by opening it in an external web browser to ensure everything is OK"

    @classmethod
    def poll(cls, context):
        scene = context.scene
        if not scene.sequence_editor:
            return False
        data = scene.bcon_video_data
        talk = data.talks[data.talk_active_index]
        talks_manager = stages.TalksManager()
        return (talk.stage == stages.STAGE_UPLOADING and
                talks_manager.talk_is_active(talk) and
                talk.is_upload_stage_validation_required)

    def execute(self, context):
        scene = context.scene
        data = scene.bcon_video_data
        talk = data.talks[data.talk_active_index]
        if not talk.is_upload_stage_validation_required:
            return {'CANCELLED'}

        talk.is_upload_stage_validation_required = False
        talk.is_upload_stage_validated = True
        return {'FINISHED'}

    def invoke(self, context, event):
        import subprocess
        scene = context.scene
        data = scene.bcon_video_data
        talk = data.talks[data.talk_active_index]
        if not talk.is_upload_stage_validation_required:
            return {'CANCELLED'}

        webbrowser.open_new_tab(talk.youtube_edit_url)

        wm = context.window_manager
        return wm.invoke_props_dialog(self)


register_classes = (
    BConVideo_install_dependencies,

    BConVideo_reset,
    BConVideo_reload,
    BConVideo_stage_next,
    BConVideo_stage_prev,

    BConVideo_frame_go,

    BConVideo_stage_acquire_files_select,
    BConVideo_talk_file_remove,

    BConVideo_stage_processing_cutframe_set,
    BConVideo_stage_processing_thumbnail_frame_set,

    BConVideo_stage_exporting_final_validate,

    BConVideo_stage_uploading_final_validate
)

